#
# File specifying the location of YODA to use.
#

set( YODA_VERSION 1.6.6 )
set( YODA_ROOT
   ${LCG_RELEASE_DIR}/MCGenerators/yoda/${YODA_VERSION}/${LCG_PLATFORM} )
